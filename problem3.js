// let inventory = require('./cars');

function alphabeticalSort(inventory)
{
    if(Array.isArray(inventory) && inventory.length != 0)
    {
    let sortArr = [];
    for(let i of inventory)
    {
        sortArr.push(i.car_model);
    }
    return sortArr.sort();
}else{
    return [];
}
}

module.exports = alphabeticalSort;