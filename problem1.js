// const inventory = require('./cars');

function matchingCarId(inventory=[], id)
{
    let carId;
    if(Array.isArray(inventory) && inventory.length != 0)
    {
        for(let i in inventory)
        {
            if(inventory[i].id === id)
            {
                carId = inventory[i];
            }    
        }
        if(carId === undefined)
        {
            return [];
        }else{
            return (carId);
        }
    }
    else{
        return [];
    }
}



module.exports = matchingCarId;